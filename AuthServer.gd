extends HTTPRequest

#onready var http = get_node("HTTPRequest")

var auth_url = "http://crawfish.ddns.net:2424/game_auth.php"

var network = NetworkedMultiplayerENet.new()
var port = 6243
var max_conn = 100
var gtwy_id = 0
var temp_sid = 0

enum RESPONSE_CODE{
	AUTH_SUCCESS,
	BAD_CREDENTIALS,
	USERNAME_ALREADY_EXISTS
}

var responses = {
	RESPONSE_CODE.AUTH_SUCCESS: "OK!",
	RESPONSE_CODE.BAD_CREDENTIALS: "Invalid username or password",
	RESPONSE_CODE.USERNAME_ALREADY_EXISTS: "Username already exists"
}

func _ready():
	#self.set_use_threads(true)
	connect("request_completed", self, "on_request_completed")
	start()

func start():
	network.create_server(port, max_conn)
	get_tree().set_network_peer(network)
	print("Auth server up and running")
	
	network.connect("peer_connected", self, "on_peer_connected")
	network.connect("peer_disconnected", self, "on_peer_disconnected")

func generate_token():
	randomize()
	var token = str(randi()).sha256_text() + "$" + str(OS.get_unix_time())
	
	return token

func send_request(username, password):
	var body = {"username": username, "password": password}
	request(auth_url, ["Content-Type: application/json"], false, HTTPClient.METHOD_POST, to_json(body))

func on_request_completed(result, response_code, headers, body):
	var response = parse_json(body.get_string_from_utf8())
	
	if response.has("error"):
		rpc_id(gtwy_id, "authentication_result", temp_sid, str(response["error"]), responses[RESPONSE_CODE.BAD_CREDENTIALS], null)
	else:
		rpc_id(gtwy_id, "authentication_result", temp_sid, str(0), responses[0], response["username"])

# REMOTES #

remote func authenticate(sid, username, password):
	print("Connection " + str(sid) + ": authentication request")
	
	temp_sid = sid
	gtwy_id = get_tree().get_rpc_sender_id()
	
	print(username, password)
	send_request(username, password)
	
	"""
	if not username == 'pinto':
		rpc_id(gtwy_id, 'authentication_result', sid, RESPONSE_CODE.BAD_CREDENTIALS, responses[RESPONSE_CODE.BAD_CREDENTIALS])
		print('Connection'  + str(sid) + ': ' + str(RESPONSE_CODE.BAD_CREDENTIALS) + ' -> ' + responses[RESPONSE_CODE.BAD_CREDENTIALS])
		return
	elif not password == '123':
		rpc_id(gtwy_id, 'authentication_result', sid, RESPONSE_CODE.BAD_CREDENTIALS, responses[RESPONSE_CODE.BAD_CREDENTIALS])
		print('Connection ' + str(sid) + ': ' + str(RESPONSE_CODE.BAD_CREDENTIALS) + ' -> ' + responses[RESPONSE_CODE.BAD_CREDENTIALS])
		return
	"""
	
	#var token = generate_token()
	#print("Connection " + str(sid) + ": Token - " + token)
	
	#rpc_id(gtwy_id, "authentication_result", sid, str(RESPONSE_CODE.AUTH_SUCCESS), responses[RESPONSE_CODE.AUTH_SUCCESS])
	
	
# REMOTES END #

func on_peer_connected(conn_id):
	print("Gateway " + str(conn_id) + " connected")
	
func on_peer_disconnected(conn_id):
	print("Gateway " + str(conn_id) + " disconnected")
